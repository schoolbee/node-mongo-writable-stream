var MongoClient = require('mongodb').MongoClient;
var Writable = require('stream').Writable;
var util = require('util');

util.inherits(MongoWritableStream, Writable);

module.exports = MongoWritableStream;

function MongoWritableStream (options) {
	if(!(this instanceof MongoWritableStream)) {
		return new MongoWritableStream(options);
	}

    this.db = null;
    this.collection = null;

	if (options.upsert == null) {
		options.upsert = false;
	}
	if (options.upsertFields == null) {
		options.upsertFields = ['_id'];
	}

    options.dbOptions = options.dbOptions || {};

	if (options.url == null) {
		throw new Error("Missing MongoDB database url in option.", options);
	}
	if (options.collection == null) {
		throw new Error("Missing MongoDB collection name in options.", options);
	}

	Writable.call(this, {
		objectMode: true,
		highWaterMark: (options.highWaterMark == null ? 16 : options.highWaterMark)
	});

	this.options = options;

    this.getCollection(function(err, collection){
        if(err){
            console.error(err);
            throw err;
        }else{
            //console.log('Connection made.', collection.collectionName);
        }
    });

}

MongoWritableStream.prototype.getDb = function getDb(callback){
    var self = this;

    if(!(typeof(callback) === 'function')){
        return;
    }

    if (self.db) {
        process.nextTick(function() {
            callback(null, self.db);
        });
    }else{
        MongoClient.connect(self.options.url, self.options.dbOptions, function(err, db) {
            if (err) {
                callback(err);
            }else {
                self.db = db;
                db.on('close', console.log);
                db.on('parseError', console.error);
                db.on('reconnect', console.log);
                db.on('timeout', console.error);
                db.on('error', console.error);
                db.on('fullsetup', console.log);
                callback(null, db);
            }
        });
    }
};

MongoWritableStream.prototype.getCollection = function getCollection(callback){
    var self = this;

    if(!(typeof(callback) === 'function')){
        return;
    }

    self.getDb(function(err, db){
        if(err){
            callback(err);
        }else {
            if (self.collection && self.collection.collectionName === self.options.collection) {
                process.nextTick(function () {
                    callback(null, self.collection);
                });
            } else {
                self.db.collection(self.options.collection, self.options.dbOptions, function (err, collection) {
                    if (err) {
                        callback(err);
                    }
                    self.collection = collection;
                    callback(null, self.collection);
                });
            }
        }
    });
};


MongoWritableStream.prototype._write = function (obj, encoding, callback) {
	var self = this;
	self.getCollection(function(err, collection){
        if(err){
            callback(err);
        }else{
            if (self.options.upsert) {
                var selector = self._getUpdateSelector(self.options.upsertFields, obj);
                self.collection.updateOne(selector, obj, { upsert: true }, callback);
            } else {
                self.collection.insertOne(obj, callback);
            }
        }
    });
};


MongoWritableStream.prototype._valueOfProperty = function (property, obj) {
	var properties = property.split('.');
	var value, p;
	for (var i = 0, ii = properties.length; i < ii; ++i) {
		p = properties[i];
		value = obj[p];
		obj = value;
	}
	return value;
};

MongoWritableStream.prototype._getUpdateSelector = function (upsertFields, obj) {
	var selector = {};
	for (var i = 0, ii = upsertFields.length; i < ii; ++i) {
		var upsertField = upsertFields[i];
		var value = this._valueOfProperty(upsertField, obj);
		if (value == null) {
			selector[upsertField] = { '$exists': false };
		} else {
			selector[upsertField] = value;
		}
	}
	return selector;
};


